Portez ce vieux whisky au juge blond qui fume sur son île intérieure, à côté de l'alcôve ovoïde, où les bûches se consument dans l'âtre, ce qui lui permet de penser à la cænogénèse de l'être dont il est question dans la cause ambiguë entendue à Moÿ, dans un capharnaüm qui, pense-t-il, diminue çà et là la qualité de son œuvre.

Dès Noël où un zéphyr haï me vêt de glaçons würmiens je dîne d’exquis rôtis de bœuf au kir à l’aÿ d’âge mûr & cætera ! (contient les 42 caractères de la langue française) (Gilles Esposito-Farèse)

Bâchez la queue du wagon-taxi avec les pyjamas du fakir.

Monsieur Jack, vous dactylographiez bien mieux que votre ami Wolf

Voix ambiguë d'un cœur qui, au zéphyr, préfère les jattes de kiwis.

Mon pauvre zébu ankylosé choque deux fois ton wagon jaune

Le vif zéphyr jubile sur les kumquats du clown gracieux

Zut ! Je crois que le chien Sambuca préfère le whisky revigorant au doux porto. 

Voyez le brick géant que j'examine près du wharf.

Portez ce vieux whisky au juge blond qui fume

Buvez de ce whisky que le patron juge fameux.

Grimpez quand ce whisky flatte vos bijoux

Whisky vert : jugez cinq fox d'aplomb.

Portons dix bons whiskys à l'avocat goujat qui fumait au zoo.

----

The quick brown fox jumps over the lazy dog

Forsaking monastic tradition, twelve jovial friars gave up their vocation for a questionable existence on the flying trapeze.

No kidding -- Lorenzo called off his trip to visit Mexico City just because they told him the conquistadores were extinct.

Jelly-like above the high wire, six quaking pachyderms kept the climax of the extravaganza in a dazzling state of flux.

